#include "IOVSvc/IOVSvc.h"
#include "IOVSvc/CondSvc.h"
#include "IOVSvc/IOVSvcTool.h"
#include "../CondInputLoader.h"
#include "../MetaInputLoader.h"

DECLARE_COMPONENT( IOVSvc )
DECLARE_COMPONENT( IOVSvcTool )

DECLARE_COMPONENT( CondSvc )

DECLARE_COMPONENT( CondInputLoader )
DECLARE_COMPONENT( MetaInputLoader )

